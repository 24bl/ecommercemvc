export async function fetchWrapper(pathname, option = { method: "GET" }) {
  const hostname = "https://jsonplaceholder.typicode.com";

  const endpoint = hostname + pathname;

  return await fetch(endpoint, { method: option.method }).then((res) =>
    res.json()
  );
}

// fetch…外から何か取ってくる
// then((res)
//GTE → POSTでもいける
