import { fetchWrapper } from "./fetchWapper.js";

//1. 処理の定義
/**
 * クリック時対象の色を変化させる。
 */
function ChangeColorOnClick(id, color) {
  //2. idが一致する要素を取得する。
  const text = document.getElementById(id);
  //3. 対象の要素が押されたときに色を変化させる。

  text.addEventListener("click", function () {
    text.style.color = color;
  });
}
// 4. 処理の実行
ChangeColorOnClick("text", "red");

/**
 * インフィニティスクロール
 * @param starRatet 処理実行位置（高さ）
 * @param add 追加する要素数
 */

function infinitScroll(startRate, add) {
  // 1. スクロールを検知
  window.addEventListener("scroll", async function () {
    // 2. ページの高さを取得
    const range =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;
    //3. 現在位置を取得
    const currentLocation = document.documentElement.scrollTop;
    const heightRate = currentLocation / range;

    if (heightRate >= startRate) {
      const list = document.getElementById("scroll");

      const userList = await fetchWrapper("/users");

      console.log(userList[0].name);

      list.append(createList(userList));
      // window.scroll({ top: 0, behavior: "smooth" });
    }
  });
}
//4. 処理の実行
infinitScroll(0.9, 8);

/*
 * ユーザ一覧の要素を作成する。
 * @param num 作成する要素の数
 */

function createList(userList) {
  // 1. 最終的に要素を格納する場所を用意
  let fragment2 = document.createDocumentFragment();

  // 2. for文で指定の個数を作成
  for (let i = 0; i < userList.length; i++) {
    // 3. 一番親になる要素
    let li = document.createElement("li");

    // 4. 3つのp要素をまとめるためのフラグメント
    let fragment = document.createDocumentFragment();

    // 5. p要素を作成
    let numberP = document.createElement("p");
    let nameP = document.createElement("p");
    let birthP = document.createElement("p");

    // 6. 挿入するテキストを作成
    const numberText = document.createTextNode(`No.` + (i + 1));
    const nameText = document.createTextNode(userList[i].name);
    const birthText = document.createTextNode("2000.10.10");

    // 7. p要素にテキストを挿入
    numberP.append(numberText);
    nameP.append(nameText);
    birthP.append(birthText);

    // 8. 3つのp要素
    fragment.append(numberP);
    fragment.append(nameP);
    fragment.append(birthP);

    // 9. liタグに3つのp要素を追加
    li.append(fragment);

    // 10. liタグをfragment2に追加
    fragment2.append(li);
  }
  return fragment2;
}

/**
 * ポップアップの開閉
 */
function popupOperation() {
  // 開閉用のボタン
  let popupButton = document.querySelector(".popup_btn");
  popupButton.addEventListener("click", () => {
    let popup = document.getElementById("popup");

    console.log(popup);

    // styleに値がないもしくはnoneの場合は"display:blovk"にそうでない場合はnoneに
    if (popup.style.display == "none" || popup.style.display == "") {
      popup.style.display = "block";
    } else {
      popup.style.display = "none";
    }
  });

  //オーバーレイの部分
  let overlay = document.querySelector(".popup_overlay");
  overlay.addEventListener("click", () => {
    let popup = document.querySelector(".popup");
    //ポップアップをとじる
    popup.style.display = "none";
  });
}
popupOperation();
