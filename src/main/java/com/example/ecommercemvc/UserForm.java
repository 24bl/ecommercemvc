package com.example.ecommercemvc;

import lombok.Data;

@Data
public class UserForm {

  // フィールド
  private String name;
  private String birth;

  // UserFormクラスをUserクラスに変換するメソッド
  public User convertToEntity() {
    return new User(name, birth);
  }

  @Override
  public String toString() {
    return "UserForm [name=" + name +
        ", birth=" + birth + "]";
  }

}