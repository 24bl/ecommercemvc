package com.example.ecommercemvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  // DI
  @Autowired
  private UserMapper userMapper;

  @Override
  public List<User> findAll() {
    List<User> userList = userMapper.findAll();
    return userList;
  }

  @Override
  public void insert(User user) {
    userMapper.insert(user);
  }
}
