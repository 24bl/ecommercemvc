package com.example.ecommercemvc;

import java.util.List;

public interface UserService {
  /**
   * 1. タスクを全件検索
   * SELECT文で複数リスト分のTodoを返す
   */
  List<User> findAll();

  /**
   * 3. タスクの追加
   * INSERT文でTodoを指定し挿入するためvoid型
   */
  void insert(User user);

}