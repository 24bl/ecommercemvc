package com.example.ecommercemvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class UserController {

  @Autowired
  private UserService userService;

  @GetMapping("/user_list")
  public String user_list(Model model) {
    List<User> userList = userService.findAll();

    model.addAttribute("userList", userList);
    return "admin/user_list";
  }

  // 入力画面に遷移
  @GetMapping("/regist")
  public String regist() {
    return "admin/regist";
  }

  // フォームに入力されたリクエストパラメータを受け取りDBへの挿入処理を行う
  // 処理後、一覧画面にリダイレクト
  @PostMapping("/insertComplete")
  public String insert(UserForm userForm) {
    User user = userForm.convertToEntity();
    userService.insert(user);
    return "redirect:user_list";
  }

  // // 仮のデータとして配列を作成
  // List<User> users = new ArrayList<User>();

  // // 値を生成し配列に追加
  // for (int i = 1; i <= 8; i++) {
  // User user = new User();
  // user.setIndex(i);
  // user.setName("山田太郎");
  // user.setBirth("2000.10.10");
  // users.add(user);
  // }
  // // htmlの"users"に上で定義したusers変数を代入する
  // mod.addAttribute("users", users);
  // return "admin/user_list";
}
