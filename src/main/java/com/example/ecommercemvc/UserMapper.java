package com.example.ecommercemvc;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {
	// 1. 全検索
	@Select("SELECT * FROM ECUser")
	List<User> findAll();

	// // 2. 主キー県検索
	// @Select({ "SELECT * FROM User", "WHERE id = #{id}" })
	// User findById(Integer id);

	// 3. 社員追加
	@Insert({ "INSERT INTO ECUser(name, birth)",
			"VALUES(#{name}, #{birth})" })
	@Options(useGeneratedKeys = true, keyColumn = "index", keyProperty = "index")
	void insert(User user);

	// // 4. 主キーで指定した社員情報の更新
	// @Update({ "UPDATE todo",
	// "SET title = #{title}, status = #{status}, priority = #{priority}",
	// "WHERE id = #{id}" })

	// int update(Todo todo);

	// // 5. 主キーで指定した社員の削除
	// @Delete({ "DELETE FROM todo WHERE id = #{id}" })
	// int delete(Integer id);

	// // 6. 優先度検索
	// // @Select({"SELECT * FROM todo", "WHERE status = #{status}"})
	// // Todo findByStatus(String status);
}