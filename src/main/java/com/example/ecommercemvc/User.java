package com.example.ecommercemvc;

import lombok.Data;

/**
 * ユーザ一覧情報
 * - インデックス番号
 * - 名前
 * - 生年月日
 */

// (lombokの機能)Getter/Setterを自動で作ってくれる
@Data // ← データを倉庫に入れたり出したりといった処理行ってくれるための表記(アノテーション)
public class User {
  private int index;
  private String name;
  private String birth;

  // Todoのデフォルトコンストラクタ
  public User() {
  }

  // FormクラスからEntityクラスに変化する際に使用するコンストラクタ
  public User(String name, String birth) {
    this.name = name;
    this.birth = birth;
  }
}